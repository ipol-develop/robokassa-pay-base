<?php

	namespace Robokassa;

	/**
	 * Class Response
	 * @package Robokassa
	 */
	class Response
	{

		/** @var double $orderPrice Сумма заказа */
		protected $orderPrice;
		/** @var string $orderId ID заказа */
		protected $orderId;
		/** @var string $crc Контрольная сумма */
		protected $crc;
		/** @var Order $order Заказ */
		protected $order;
		/** @var Configure Настройки robokassa */
		protected $configure;
		/**
		 * Дополнительные пользовательские параметры
		 * @var array $shpFields
		 *
		 * Они также относятся к необязательным параметрам,
		 * но несут совершенно другую смысловую нагрузку.
		 * Это такие параметры, которые ROBOKASSA никак не обрабатывает,
		 * но всегда возвращает магазину в ответных вызовах.
		 */
		protected $shpFields = [];

		/**
		 * @return double
		 */
		public function getOrderPrice()
		{
			return $this->orderPrice;
		}

		/**
		 * @param double $orderPrice
		 * @return self
		 */
		public function setOrderPrice($orderPrice)
		{
			$this->orderPrice = $orderPrice;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getOrderId()
		{
			return $this->orderId;
		}

		/**
		 * @param string $orderId
		 * @return self
		 */
		public function setOrderId($orderId)
		{
			$this->orderId = $orderId;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getCrc()
		{
			return $this->crc;
		}

		/**
		 * @param string $crc
		 * @return self
		 */
		public function setCrc($crc)
		{
			$this->crc = $crc;
			return $this;
		}

		/**
		 * @return Order
		 */
		public function getOrder()
		{
			return $this->order;
		}

		/**
		 * @param Order $order
		 * @return self
		 */
		public function setOrder($order)
		{
			$this->order = $order;
			return $this;
		}

		/**
		 * @return Configure
		 */
		public function getConfigure()
		{
			return $this->configure;
		}

		/**
		 * @param Configure $configure
		 * @return self
		 */
		public function setConfigure($configure)
		{
			$this->configure = $configure;
			return $this;
		}

		/**
		 * @return array
		 */
		public function getShpFields()
		{
			return $this->shpFields;
		}

		/**
		 * @param array $shpFields
		 * @return self
		 */
		public function setShpFields($shpFields)
		{
			$this->shpFields = $shpFields;
			return $this;
		}

		/**
		 * Проверка подписи результата платежной системы
		 * @return bool
		 */
		public function checkPayCrc()
		{

			/** @var array $params */
			$params = [
				'crc' => (string) \strtoupper($this->getCrc())
			];

			$params['calcCrc'] = (string) \strtoupper(
				\md5(
					\implode(
						':',
						[
							$this->getOrderPrice(),
							$this->getOrderId(),
							$this->configure->getPassword2()
						]
					)
				)
			);

			return $params['crc'] === $params['calcCrc'];
		}

		/**
		 * Проверка оплаты заказа
		 * @return bool
		 */
		public function isPayed()
		{
			return $this->order->getOrderId() == $this->getOrderId()
				&& $this->order->getPrice() == $this->getOrderPrice()
				&& $this->checkPayCrc()
			;
		}

		/**
		 * Ответ робокассе об ошибке оплаты
		 * @return string
		 */
		public function failResponse()
		{
			return "bad sign\n";
		}

		/**
		 * Ответ робокассе об успешной оплате
		 * @return string
		 */
		public function successResponse()
		{
			return "OK" . $this->getOrderId() . "\n";
		}
	}