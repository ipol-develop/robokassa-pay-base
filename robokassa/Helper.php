<?php

	namespace Robokassa;

	/**
	 * Class Helper
	 * @package Robokassa
	 */
	class Helper
	{

		/**
		 * Получение доступных методов оплаты для магазина
		 * @param Configure $configure
		 * @return array
		 */
		public static function getPayMethod(Configure $configure)
		{

			/** @var array $result */
			$result = [];

			try
			{
				/** @var string $xmlDocument */
				$xmlDocument = \file_get_contents(
					'https://auth.robokassa.ru/Merchant/WebService/Service.asmx/GetCurrencies?MerchantLogin='
					. $configure->getLogin()
					. '&Language=ru'
				);

				/** @var \SimpleXMLElement $xml */
				$xml = \simplexml_load_string($xmlDocument);

				if(
					!isset($xml->Groups)
					|| !isset($xml->Groups->Group)
					|| (int) $xml->Groups->Group->count() === 0
				)
					return $result;

				/** @var \SimpleXMLElement $group */
				foreach($xml->Groups->Group as $group)
				{

					if(
						!isset($group->Items)
						|| !isset($group->Items->Currency)
						|| (int) $group->Items->Currency->count() === 0
					)
						continue;

					/** @var array $item */
					$item = [
						'code' => $group->attributes()->Code->__toString(),
						'title' => $group->attributes()->Description->__toString(),
						'items' => [],
					];

					/** @var \SimpleXMLElement $currency*/
					foreach($group->Items->Currency as $currency)
					{

						/** @var array $labels */
						$labels = ['Label', 'Alias', 'Name', 'MinValue', 'MaxValue'];

						/** @var array $currencyUnit */
						$currencyUnit = [];

						foreach($labels as $label)
						{
							if(isset($currency->attributes()->$label))
							{
								$currencyUnit[$label] = $currency->attributes()->$label->__toString();
							}
						}

						$item['items'][] = $currencyUnit;
					}

					if((int) \count($item['items']) > 0)
						$result[] = $item;
				}
			}catch (\Exception $exception)
			{

				return [];
			}

			return $result;
		}

		/**
		 * Получение URL для оплаты
		 * @return string
		 */
		public static function getPaymentUrl()
		{
			return Payment::BASE_PAYMENT_URL;
		}

		/**
		 * Получение типов оплат для физ лица с учетом комиссии
		 * @param Configure $configure
		 * @param Order $order
		 * @return array
		 */
		public static function getPaymentsWithOrderCommission(Configure $configure, Order $order)
		{

			/** @var array $result */
			$result = [];

			/** @var string $xmlDocument */
			$xmlDocument = \file_get_contents(
				'https://auth.robokassa.ru/Merchant/WebService/Service.asmx/GetRates?MerchantLogin='
				. $configure->getLogin()
				. '&OutSum='
				. $order->getPrice()
				. '&Language=ru'
			);

			/** @var \SimpleXMLElement $xml */
			$xml = \simplexml_load_string($xmlDocument);

			foreach($xml->Groups->Group as $group)
			{

				$result[$group->attributes()->Code->__toString()] = [
					'name' => $group->attributes()->Description->__toString(),
					'code' => $group->attributes()->Code->__toString(),
					'items' => [],
				];

				foreach($group->Items->Currency as $currency)
				{

					/** @var float $price */
					$price = 0;

					switch ($configure->getWhoPayCommission())
					{
						case Payment::WHO_PAY_COMMISSION_BUYER:
							$price = $currency->Rate->attributes()->IncSum->__toString();
						break;
						case Payment::WHO_PAY_COMMISSION_SHOP:
							$price = $order->getPrice();
						break;
					}

					$result[$group->attributes()->Code->__toString()]['items'][] = [
						'name' => $currency->attributes()->Name->__toString(),
						'alias' => $currency->attributes()->Alias->__toString(),
						'label' => $currency->attributes()->Label->__toString(),
						'price' => $price,
					];
				}
			}

			return $result;
		}

		/**
		 * Получение цены заказа без комиссии
		 * @param Configure $configure
		 * @param Order $order
		 * @param $IncCurrLabel
		 * @return float
		 */
		public static function getOrderPriceWithOutCommission(Configure $configure, Order $order, $IncCurrLabel)
		{

			/** @var string $xmlDocument */
			$xmlDocument = \file_get_contents(
				'https://auth.robokassa.ru/Merchant/WebService/Service.asmx/CalcOutSumm?MerchantLogin='
				. $configure->getLogin()
				. '&IncSum='
				. $order->getPrice()
				. '&IncCurrLabel='
				. $IncCurrLabel
				. '&Language=ru'
			);

			/** @var \SimpleXMLElement $xml */
			$xml = simplexml_load_string($xmlDocument);

			return (float) $xml->OutSum->__toString();
		}
	}