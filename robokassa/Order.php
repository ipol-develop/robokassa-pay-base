<?php

	namespace Robokassa;

	/**
	 * Class Order
	 * @package Robokassa
	 */
	class Order
	{

		/** @var string $orderId Номер заказа */
		protected $orderId;
		/** @var string $price Сумма заказа */
		protected $price;
		/** @var Product[] $products Товара заказа */
		protected $products;
		/**
		 * Дополнительные пользовательские параметры
		 * @var array $shpFields
		 *
		 * Они также относятся к необязательным параметрам,
		 * но несут совершенно другую смысловую нагрузку.
		 * Это такие параметры, которые ROBOKASSA никак не обрабатывает,
		 * но всегда возвращает магазину в ответных вызовах.
		 */
		protected $shpFields = [];

		/**
		 * Добавление товара к заказу
		 * @param Product $product
		 * @return self
		 */
		public function addProduct(Product $product)
		{
			$this->products[] = $product;
			return $this;
		}

		/**
		 * Получение данных для чеков
		 * @param Configure $configure
		 * @return array
		 */
		public function getBasketReceipt(Configure $configure)
		{

			/** @var array $result */
			$result = [];

			/** @var Product $product */
			foreach($this->getProducts() as $product)
			{
				$result[] = [
					'name' => \mb_substr($product->getName(), 0, 63),
					'quantity' => \sprintf("%01.3f", (double) $product->getQuantity()),
					'sum' => \sprintf("%01.2f", (double) ($product->getPrice() * $product->getQuantity())),
					'tax' => $product->getVat() === null ? $configure->getVat() : $product->getVat(),
					'payment_method' => $product->getPaymentMethod(),
					'payment_object' => $product->getPaymentObject(),
				];
			}

			return $result;
		}

		/**
		 * @return string
		 */
		public function getOrderId()
		{
			return $this->orderId;
		}

		/**
		 * @param string $orderId
		 * @return self
		 */
		public function setOrderId($orderId)
		{
			$this->orderId = $orderId;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getPrice()
		{
			return $this->price;
		}

		/**
		 * @param string $price
		 * @return self
		 */
		public function setPrice($price)
		{
			$this->price = $price;
			return $this;
		}

		/**
		 * @return Product[]
		 */
		public function getProducts()
		{
			return $this->products;
		}

		/**
		 * @return array
		 */
		public function getShpFields()
		{
			return $this->shpFields;
		}

		/**
		 * @param array $shpFields
		 * @return self
		 */
		public function setShpFields(array $shpFields)
		{

			$this->shpFields = [];

			foreach($shpFields as $code => $value)
				$this->shpFields['Shp_' . $code] = $value;

			return $this;
		}
	}