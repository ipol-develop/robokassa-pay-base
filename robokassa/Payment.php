<?php

	namespace Robokassa;

	/**
	 * Class Payment
	 * @package Robokassa
	 */
	class Payment
	{

		/** @var string HTTP METHOD POST */
		const METHOD_POST = 'post';
		/** @var string HTTP METHOD GET */
		const METHOD_GET = 'get';

		/** Базовый url для перенаправления на оплату */
		const BASE_PAYMENT_URL = 'https://auth.robokassa.ru/Merchant/Index.aspx?';

		/** ФЗ-54 : Лояльный */
		const FISCAL_SERVICE_LOCAL = 'local';
		/** ФЗ-54 : Облачный */
		const FISCAL_SERVICE_CLOUD = 'cloud';
		/** ФЗ-54 : Робомаркет */
		const FISCAL_SERVICE_ROBO_MARKET = 'robotmarket';

		/** «osn» – общая СН */
		const SNO_OSN = 'osn';
		/** «usn_income» – упрощенная СН (доходы) */
		const SNO_USN_INCOME = 'usn_income';
		/** «usn_income_outcome» – упрощенная СН (доходы минус расходы) */
		const SNO_USN_INCOME_OUTCOME = 'usn_income_outcome';
		/** «envd» – единый налог на вмененный доход */
		const SNO_ENVD = 'envd';
		/** «esn» – единый сельскохозяйственный налог */
		const SNO_ESN = 'esn';
		/** «patent» – патентная СН */
		const SNO_PARENT = 'patent';

		/** @var string без НДС */
		const VAT_NONE = 'none';
		/** @var string НДС по ставке 0% */
		const VAT_VAT0 = 'vat0';
		/** @var string НДС по ставке 10% */
		const VAT_VAT10 = 'vat10';
		/** @var string НДС чека по ставке 18% */
		const VAT_VAT18 = 'vat18';
        /** @var string НДС чека по ставке 20% */
		const VAT_VAT20 = 'vat20';
		/** @var string НДС чека по расчетной ставке 10/110 */
		const VAT_VAT110 = 'vat110';
		/** @var string НДС чека по расчетной ставке 18/118 */
		const VAT_VAT118 = 'vat118';
        /** @var string НДС чека по расчетной ставке 20/120 */
		const VAT_VAT120 = 'vat120';

		/** @var string предоплата 100%. Полная предварительная оплата до момента передачи предмета расчета */
		const PAYMENT_METHOD_FULL_PREPAYMENT = 'full_prepayment';
		/** @var string предоплата. Частичная предварительная оплата до момента передачи предмета расчета */
		const PAYMENT_METHOD_PREPAYMENT = 'prepayment';
		/** @var string аванс */
		const PAYMENT_METHOD_ADVANCE = 'advance';
		/** @var string полный расчет. Полная оплата, в том числе с учетом аванса (предварительной оплаты) в момент передачи предмета расчета */
		const PAYMENT_METHOD_FULL_PAYMENT = 'full_payment';
		/** @var string частичный расчет и кредит. Частичная оплата предмета расчета в момент его передачи с последующей оплатой в кредит */
		const PAYMENT_METHOD_PARTIAL_PAYMENT = 'partial_payment';
		/** @var string передача в кредит. Передача предмета расчета без его оплаты в момент его передачи с последующей оплатой в кредит */
		const PAYMENT_METHOD_CREDIT = 'credit';
		/** @var string оплата кредита. Оплата предмета расчета после его передачи с оплатой в кредит (оплата кредита) */
		const PAYMENT_METHOD_CREDIT_PAYMENT = 'credit_payment';

		/** @var array $paymentMethod Признак способа расчёта */
		public static $paymentMethods = [
			self::PAYMENT_METHOD_FULL_PREPAYMENT => [
				'title' => 'Предоплата 100%',
				'code' => self::PAYMENT_METHOD_FULL_PREPAYMENT,
			],
			self::PAYMENT_METHOD_PREPAYMENT => [
				'title' => 'Частичная предоплата',
				'code' => self::PAYMENT_METHOD_PREPAYMENT,
			],
			self::PAYMENT_METHOD_ADVANCE => [
				'title' => 'Аванас',
				'code' => self::PAYMENT_METHOD_ADVANCE,
			],
			self::PAYMENT_METHOD_FULL_PAYMENT => [
				'title' => 'Полный расчет',
				'code' => self::PAYMENT_METHOD_FULL_PAYMENT,
			],
			self::PAYMENT_METHOD_PARTIAL_PAYMENT => [
				'title' => 'Частичный расчет и кредит',
				'code' => self::PAYMENT_METHOD_PARTIAL_PAYMENT,
			],
			self::PAYMENT_METHOD_CREDIT => [
				'title' => 'Передача в кредит',
				'code' => self::PAYMENT_METHOD_CREDIT,
			],
			self::PAYMENT_METHOD_CREDIT_PAYMENT => [
				'title' => 'Оплата кредита',
				'code' => self::PAYMENT_METHOD_CREDIT_PAYMENT,
			],
		];

		/** @var string товар. О реализуемом товаре, за исключением подакцизного товара (наименование и иные сведения, описывающие товар) */
		const PAYMENT_OBJECT_COMMODITY = 'commodity';
		/** @var string подакцизный товар. О реализуемом подакцизном товаре (наименование и иные сведения, описывающие товар) */
		const PAYMENT_OBJECT_EXCISE = 'excise';
		/** @var string работа. О выполняемой работе (наименование и иные сведения, описывающие работу) */
		const PAYMENT_OBJECT_JOB = 'job';
		/** @var string услуга. Об оказываемой услуге (наименование и иные сведения, описывающие услугу) */
		const PAYMENT_OBJECT_SERVICE = 'service';
		/** @var string ставка азартной игры. О приеме ставок при осуществлении деятельности по проведению азартных игр */
		const PAYMENT_OBJECT_GAMBLING_BET = 'gambling_bet';
		/** @var string выигрыш азартной игры. О выплате денежных средств в виде выигрыша при осуществлении деятельности по проведению азартных игр */
		const PAYMENT_OBJECT_GAMBLING_PRIZE = 'gambling_prize';
		/** @var string лотерейный билет. О приеме денежных средств при реализации лотерейных билетов, электронных лотерейных билетов, приеме лотерейных ставок при осуществлении деятельности по проведению лотерей */
		const PAYMENT_OBJECT_LOTTERY = 'lottery';
		/** @var string  выигрыш лотереи. О выплате денежных средств в виде выигрыша при осуществлении деятельности по проведению лотерей */
		const PAYMENT_OBJECT_LOTTERY_PRIZE = 'lottery_prize';
		/** @var string предоставление результатов интеллектуальной деятельности. О предоставлении прав на использование результатов интеллектуальной деятельности или средств индивидуализации */
		const PAYMENT_OBJECT_INTELLECTUAL_ACTIVITY = 'intellectual_activity';
		/** @var string платеж. Об авансе, задатке, предоплате, кредите, взносе в счет оплаты, пени, штрафе, вознаграждении, бонусе и ином аналогичном предмете расчета */
		const PAYMENT_OBJECT_PAYMENT = 'payment';
		/** @var string агентское вознаграждение. О вознаграждении пользователя, являющегося платежным агентом (субагентом), банковским платежным агентом (субагентом), комиссионером, поверенным или иным агентом */
		const PAYMENT_OBJECT_AGENT_COMMISSION = 'agent_commission';
		/** @var string составной предмет расчета. О предмете расчета, состоящем из предметов, каждому из которых может быть присвоено значение выше перечисленных признаков */
		const PAYMENT_OBJECT_COMPOSITE = 'composite';
		/** @var string иной предмет расчета. О предмете расчета, не относящемуся к выше перечисленным предметам расчета */
		const PAYMENT_OBJECT_ANOTHER = 'another';

		/** @var array $paymentObject Признак предмета расчёта */
		public static $paymentObjects = [
			self::PAYMENT_OBJECT_COMMODITY => [
				'title' => 'Товар',
				'code' => self::PAYMENT_OBJECT_COMMODITY,
			],
			self::PAYMENT_OBJECT_EXCISE => [
				'title' => 'Подакцизный товар',
				'code' => self::PAYMENT_OBJECT_EXCISE,
			],
			self::PAYMENT_OBJECT_JOB => [
				'title' => 'Работа',
				'code' => self::PAYMENT_OBJECT_JOB,
			],
			self::PAYMENT_OBJECT_SERVICE => [
				'title' => 'Услуга',
				'code' => self::PAYMENT_OBJECT_SERVICE,
			],
			self::PAYMENT_OBJECT_GAMBLING_BET => [
				'title' => 'Ставка азартной игры',
				'code' => self::PAYMENT_OBJECT_GAMBLING_BET,
			],
			self::PAYMENT_OBJECT_GAMBLING_PRIZE => [
				'title' => 'Выигрыш азартной игры',
				'code' => self::PAYMENT_OBJECT_GAMBLING_PRIZE,
			],
			self::PAYMENT_OBJECT_LOTTERY => [
				'title' => 'Лотерейный билет',
				'code' => self::PAYMENT_OBJECT_LOTTERY,
			],
			self::PAYMENT_OBJECT_LOTTERY_PRIZE => [
				'title' => 'Выигрыш лотереи',
				'code' => self::PAYMENT_OBJECT_LOTTERY_PRIZE,
			],
			self::PAYMENT_OBJECT_INTELLECTUAL_ACTIVITY => [
				'title' => 'Предоставление результатов интеллектуальной деятельности',
				'code' => self::PAYMENT_OBJECT_INTELLECTUAL_ACTIVITY,
			],
			self::PAYMENT_OBJECT_PAYMENT => [
				'title' => 'Платеж',
				'code' => self::PAYMENT_OBJECT_PAYMENT,
			],
			self::PAYMENT_OBJECT_AGENT_COMMISSION => [
				'title' => 'Агентское вознаграждение',
				'code' => self::PAYMENT_OBJECT_AGENT_COMMISSION,
			],
			self::PAYMENT_OBJECT_COMPOSITE => [
				'title' => 'Составной предмет расчета',
				'code' => self::PAYMENT_OBJECT_COMPOSITE,
			],
			self::PAYMENT_OBJECT_ANOTHER => [
				'title' => 'Иной предмет расчета',
				'code' => self::PAYMENT_OBJECT_ANOTHER,
			],
		];

		/** @var array $vats */
		public static $vats = [

			self::VAT_NONE => [
				'title' => 'без НДС',
				'code' => self::VAT_NONE,
			],
			self::VAT_VAT0 => [
				'title' => 'НДС по ставке 0%',
				'code' => self::VAT_VAT0,
			],
			self::VAT_VAT10 => [
				'title' => 'НДС чека по ставке 10%',
				'code' => self::VAT_VAT10,
			],
			self::VAT_VAT18 => [
				'title' => 'НДС чека по ставке 18%',
				'code' => self::VAT_VAT18,
			],
			self::VAT_VAT20 => [
				'title' => 'НДС чека по ставке 20%',
				'code' => self::VAT_VAT20,
			],
			self::VAT_VAT110 => [
				'title' => 'НДС чека по расчетной ставке 10/110',
				'code' => self::VAT_VAT110,
			],
			self::VAT_VAT118 => [
				'title' => 'НДС чека по расчетной ставке 18/118',
				'code' => self::VAT_VAT118,
			],
			self::VAT_VAT120 => [
				'title' => 'НДС чека по расчетной ставке 20/120',
				'code' => self::VAT_VAT120,
			],
		];

		/** @var array $fiscalServices */
		public static $fiscalServices = [

			self::FISCAL_SERVICE_CLOUD => [
				'title' => 'Робочеки, Облачное, Кассовое',
				'code' => self::FISCAL_SERVICE_CLOUD,
			],

			self::FISCAL_SERVICE_LOCAL => [
				'title' => 'Не передавать номенклатуру',
				'code' => self::FISCAL_SERVICE_LOCAL,
			],
		];

		/** @var array $sno */
		public static $snos = [

			self::SNO_OSN => [
				'title' => 'общая СН',
				'code' => self::SNO_OSN,
			],
			self::SNO_USN_INCOME => [
				'title' => 'упрощенная СН (доходы)',
				'code' => self::SNO_USN_INCOME,
			],
			self::SNO_USN_INCOME_OUTCOME => [
				'title' => 'упрощенная СН (доходы минус расходы)',
				'code' => self::SNO_USN_INCOME_OUTCOME,
			],
			self::SNO_ENVD => [
				'title' => 'единый налог на вмененный доход',
				'code' => self::SNO_ENVD,
			],
			self::SNO_ESN => [
				'title' => 'единый сельскохозяйственный налог',
				'code' => self::SNO_ESN,
			],
			self::SNO_PARENT => [
				'title' => 'патентная СН',
				'code' => self::SNO_PARENT,
			],
		];

		/** @var string Страна магазина Россия */
		const COUNTRY_CODE_RU = 'RU';
		/** @var string Страна магазина Казахстан */
		const COUNTRY_CODE_KZ = 'KZ';

		/** @var string Валюта передачи суммы заказа Рубли */
		const OUT_SUM_CURRENCY_RUB = '';
		/** @var string Валюта передачи суммы заказа Доллоры */
		const OUT_SUM_CURRENCY_USD = 'USD';
		/** @var string Валюта передачи суммы заказа Евро */
		const OUT_SUM_CURRENCY_EUR = 'EUR';
		/** @var string Валюта передачи суммы заказа Тенге */
		const OUT_SUM_CURRENCY_KZT = 'KZT';

		/** @var string Тип магахина - Физическое лицо */
		const SHOP_TYPE_INDIVIDUAL = 'individual';
		/** @var string Тип магахина - Юридическое лицо */
		const SHOP_TYPE_ORGANIZATION = 'organization';

		/** @var array $shopTypes */
		public static $shopTypes = [
			self::SHOP_TYPE_INDIVIDUAL => [
				'title' => 'Физическое лицо',
				'code' => self::SHOP_TYPE_INDIVIDUAL,
			],
			self::SHOP_TYPE_ORGANIZATION => [
				'title' => 'Юридическое лицо',
				'code' => self::SHOP_TYPE_ORGANIZATION,
			],
		];

		/** @var string Коммиссию платит магазин */
		const WHO_PAY_COMMISSION_SHOP = 'commission-pay-shop';
		/** @var string Коммиссию платит покупатель */
		const WHO_PAY_COMMISSION_BUYER = 'commission-pay-buyer';

		/** @var array $commissionPayers */
		public static $commissionPayers = [
			self::WHO_PAY_COMMISSION_SHOP => [
				'title' => 'Магазин',
				'code' => self::WHO_PAY_COMMISSION_SHOP,
			],
			self::WHO_PAY_COMMISSION_BUYER => [
				'title' => 'Покупатель',
				'code' => self::WHO_PAY_COMMISSION_BUYER,
			],
		];

		/** @var string Язык интерфейса оплаты - русский */
		const CULTURE_RU = 'ru';
		/** @var string Язык интерфейса оплаты - английский */
		const CULTURE_EN = 'en';

		/**
		 * Получение сформировонного массива для оплаты
		 * @param Configure $configure
		 * @param Order $order
		 * @return array
		 * @throws \Exception
		 */
		public static function getOrderPayFields(Configure $configure, Order $order)
		{

			if($configure->getShopType() !== self::SHOP_TYPE_ORGANIZATION)
				throw new \Exception('only shop call method getOrderPayFields');

			/** @var array $crcParams */
			$crcParams = [];

			/** @var array $urlRequest */
			$urlRequest = [
				'MrchLogin' => $configure->getLogin(),
				'OutSum' => $order->getPrice(),
				'InvId' => $order->getOrderId(),
			];

			/** Платежная система по умолчанию */
			if(\mb_strlen($configure->getDefaultPayMethod()) > 0)
				$urlRequest['IncCurrLabel'] = $configure->getDefaultPayMethod();

			$crcParams[] = $configure->getLogin();
			$crcParams[] = $order->getPrice();
			$crcParams[] = $order->getOrderId();

			if($configure->getOutSumCurrency() !== Payment::OUT_SUM_CURRENCY_RUB)
			{
				$urlRequest['OutSumCurrency'] = $configure->getOutSumCurrency();
				$crcParams[] = $configure->getOutSumCurrency();
			}

			/** Отправка товаров в коризину для фискалки облачной Атола */
			if(
				$configure->getFiscalService() === Payment::FISCAL_SERVICE_CLOUD
				&& $configure->getCountryCode() === Payment::COUNTRY_CODE_RU
			)
			{
				$urlRequest['Receipt'] = $crcParams[] = \json_encode(
					[
						'sno' => $configure->getSno(),
						'items' => $order->getBasketReceipt($configure)
					]
				);
			}

			$crcParams[] = $configure->getPassword1();

			if((int) \count($order->getShpFields()) > 0)
			{
				/** @var array $shpParams */
				$shpParams = $order->getShpFields();

				\ksort($shpParams);

				$crcParams = \array_merge(
					$crcParams,
					\array_map(
						function ($key, $value)
						{
							return \implode(
								'=',
								[
									$key,
									$value
								]
							);
						},
						\array_keys($shpParams), \array_values($shpParams)
					)
				);

				$urlRequest = \array_merge($urlRequest, $shpParams);
			}

			$urlRequest['SignatureValue'] = \md5(\implode(':', $crcParams));

			if($configure->isTestMode())
				$urlRequest['IsTest'] = 1;

			$urlRequest['culture'] = $configure->getCulture();

			if(empty($urlRequest['culture']) || (int) \mb_strlen($urlRequest['culture']) === 0)
				$urlRequest['culture'] = self::CULTURE_RU;

			return $urlRequest;
		}

		/**
		 * Получение сформировонного массива для оплаты физического лица
		 * @param Configure $configure
		 * @param Order $order
		 * @return array
		 * @throws \Exception
		 */
		public static function getOrderPayFieldsIndividual(Configure $configure, Order $order)
		{

			if($configure->getShopType() !== self::SHOP_TYPE_INDIVIDUAL)
				throw new \Exception('only individual call method getOrderPayFieldsIndividual');

			switch ($configure->getWhoPayCommission())
			{

				case Payment::WHO_PAY_COMMISSION_SHOP:
				default:

					/** @var float $orderPrice */
					$orderPrice = Helper::getOrderPriceWithOutCommission($configure, $order, $configure->getDefaultPayMethod());
				break;

				case Payment::WHO_PAY_COMMISSION_BUYER:

					/** @var float $orderPrice */
					$orderPrice = $order->getPrice();
				break;
			}

			/** @var array $crcParams */
			$crcParams = [
				$configure->getLogin(),
				$orderPrice,
				$order->getOrderId(),
			];

			/** @var array $urlRequest */
			$urlRequest = [
				'MrchLogin' => $configure->getLogin(),
				'OutSum' => $orderPrice,
				'InvId' => $order->getOrderId(),
				'IncCurrLabel' => $configure->getDefaultPayMethod(),
			];

			if($configure->getOutSumCurrency() !== Payment::OUT_SUM_CURRENCY_RUB)
			{
				$urlRequest['OutSumCurrency'] = $configure->getOutSumCurrency();
				$crcParams[] = $configure->getOutSumCurrency();
			}

			$crcParams[] = $configure->getPassword1();

			if((int) \count($order->getShpFields()) > 0)
			{
				/** @var array $shpParams */
				$shpParams = $order->getShpFields();

				\ksort($shpParams);

				$crcParams = \array_merge(
					$crcParams,
					\array_map(
						function ($key, $value)
						{
							return \implode(
								'=',
								[
									$key,
									$value
								]
							);
						},
						\array_keys($shpParams), \array_values($shpParams)
					)
				);

				$urlRequest = \array_merge($urlRequest, $shpParams);
			}

			$urlRequest['SignatureValue'] = \md5(\implode(':', $crcParams));

			if($configure->isTestMode())
				$urlRequest['IsTest'] = 1;

			$urlRequest['culture'] = $configure->getCulture();

			if(empty($urlRequest['culture']) || (int) \mb_strlen($urlRequest['culture']) === 0)
				$urlRequest['culture'] = self::CULTURE_RU;

			return $urlRequest;
		}

		/**
		 * Получение ссылки на оплату (метод GET лучше не использовать для передачи корзины)
		 * @param Configure $configure
		 * @param Order $order
		 * @return string
		 * @throws \Exception
		 */
		public static function getPayUrl(Configure $configure, Order $order)
		{
			return Helper::getPaymentUrl() . \http_build_query(self::getOrderPayFields($configure, $order));
		}

		/**
		 * Обработка ответа Robokass (resultUrl)
		 * @param Configure $configure
		 * @param Order $order
		 * @param string $method
		 * @return Response
		 */
		public static function processOrderResult(Configure $configure, Order $order, $method = self::METHOD_POST)
		{

			/** @var Response $response */
			$response = new Response();

			$response->setOrderPrice($method === self::METHOD_POST ? $_POST['OutSum'] : $_GET['OutSum'])
				->setOrderId($method === self::METHOD_POST ? $_POST['InvId'] : $_GET['InvId'])
				->setCrc($method === self::METHOD_POST ? $_POST['SignatureValue'] : $_GET['SignatureValue'])
				->setOrder($order)
				->setConfigure($configure);

			/** @var array $fields */
			$fields = [];

			switch ($method)
			{

				case self::METHOD_POST:

					foreach($_POST as $code => $value)
					{
						if(\preg_match('/Shp_(.*)/', $code, $matches))
						{
							$fields[$matches[1]] = $value;
						}
					}
					break;

				case self::METHOD_GET:

					foreach($_GET as $code => $value)
					{
						if(\preg_match('/Shp_(.*)/', $code, $matches))
						{
							$fields[$matches[1]] = $value;
						}
					}
					break;
			}

			if((int) \count($fields) > 0)
				$response->setShpFields($fields);

			return $response;
		}
	}