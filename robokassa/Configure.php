<?php

	namespace Robokassa;

	/**
	 * Class Configure
	 * @package Robokassa
	 */
	class Configure
	{

		/** @var string $login Идентификатор магазина */
		protected $login;
		/** @var string $password1 Пароль #1 */
		protected $password1;
		/** @var string $password2 Пароль #2 */
		protected $password2;
		/** @var bool $testMode Тестовый режим */
		protected $testMode = false;
		/** @var string $testPassword1 Тестовый пароль #1 */
		protected $testPassword1;
		/** @var string $testPassword2 Тестовый пароль #2 */
		protected $testPassword2;
		/** @var string $defaultPayMethod Метод оплаты по умолчанию */
		protected $defaultPayMethod;
		/** @var string $sno Система налогооблажения */
		protected $sno;
		/** @var string $fiscalService Режим фискальной службы */
		protected $fiscalService;
		/** @var string $vat НДС */
		protected $vat;
		/** @var string $countryCode Страна магазина */
		protected $countryCode = Payment::COUNTRY_CODE_RU;
		/** @var string $outSumCurrency Валюта передачи заказа */
		protected $outSumCurrency = Payment::OUT_SUM_CURRENCY_RUB;
		/** @var string $shopType Тип магазина, физ / юр */
		protected $shopType = Payment::SHOP_TYPE_ORGANIZATION;
		/** @var string $whoPayCommission Кто платит комиссию, магазин / покупатель */
		protected $whoPayCommission = Payment::WHO_PAY_COMMISSION_SHOP;
		/** @var string $culture Язык страницы оплаты */
		protected $culture = '';

		/**
		 * @return string
		 */
		public function getLogin()
		{
			return $this->login;
		}

		/**
		 * @param string $login
		 * @return self
		 */
		public function setLogin($login)
		{
			$this->login = $login;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getPassword1()
		{
			return $this->isTestMode() ? $this->testPassword1 : $this->password1;
		}

		/**
		 * @param string $password1
		 * @return self
		 */
		public function setPassword1($password1)
		{
			$this->password1 = $password1;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getPassword2()
		{
			return $this->isTestMode() ? $this->testPassword2 : $this->password2;
		}

		/**
		 * @param string $password2
		 * @return self
		 */
		public function setPassword2($password2)
		{
			$this->password2 = $password2;
			return $this;
		}

		/**
		 * @return bool
		 */
		public function isTestMode()
		{
			return $this->testMode;
		}

		/**
		 * @param bool $testMode
		 * @return self
		 */
		public function setTestMode($testMode)
		{
			$this->testMode = $testMode;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getTestPassword1()
		{
			return $this->testPassword1;
		}

		/**
		 * @param string $testPassword1
		 * @return self
		 */
		public function setTestPassword1($testPassword1)
		{
			$this->testPassword1 = $testPassword1;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getTestPassword2()
		{
			return $this->testPassword2;
		}

		/**
		 * @param string $testPassword2
		 * @return self
		 */
		public function setTestPassword2($testPassword2)
		{
			$this->testPassword2 = $testPassword2;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getDefaultPayMethod()
		{
			return $this->defaultPayMethod;
		}

		/**
		 * @param string $defaultPayMethod
		 * @return self
		 */
		public function setDefaultPayMethod($defaultPayMethod)
		{
			$this->defaultPayMethod = $defaultPayMethod;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getSno()
		{
			return $this->sno;
		}

		/**
		 * @param string $sno
		 * @return self
		 */
		public function setSno($sno)
		{
			$this->sno = $sno;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getFiscalService()
		{
			return $this->fiscalService;
		}

		/**
		 * @param string $fiscalService
		 * @return self
		 */
		public function setFiscalService($fiscalService)
		{
			$this->fiscalService = $fiscalService;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getVat()
		{
			return $this->vat;
		}

		/**
		 * @param string $vat
		 * @return self
		 */
		public function setVat($vat)
		{
			$this->vat = $vat;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getCountryCode()
		{
			return $this->countryCode;
		}

		/**
		 * @param string $countryCode
		 * @return self
		 */
		public function setCountryCode($countryCode)
		{
			$this->countryCode = $countryCode;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getOutSumCurrency()
		{
			return $this->outSumCurrency;
		}

		/**
		 * @param string $outSumCurrency
		 * @return self
		 */
		public function setOutSumCurrency($outSumCurrency)
		{
			$this->outSumCurrency = $outSumCurrency;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getShopType()
		{
			return $this->shopType;
		}

		/**
		 * @param string $shopType
		 * @return self
		 */
		public function setShopType($shopType)
		{
			$this->shopType = $shopType;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getWhoPayCommission()
		{
			return $this->whoPayCommission;
		}

		/**
		 * @param string $whoPayCommission
		 * @return self
		 */
		public function setWhoPayCommission($whoPayCommission)
		{
			$this->whoPayCommission = $whoPayCommission;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getCulture()
		{
			return $this->culture;
		}

		/**
		 * @param string $culture
		 * @return self
		 */
		public function setCulture($culture)
		{
			$this->culture = $culture;
			return $this;
		}
	}