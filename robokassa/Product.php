<?php

	namespace Robokassa;

	/**
	 * Class Product
	 * @package Robokassa
	 */
	class Product
	{

		/** @var string $name Название товара */
		protected $name;
		/** @var double $price Цена товара */
		protected $price;
		/** @var double $quantity Количество товара */
		protected $quantity;
		/** @var string $vat НДС */
		protected $vat = null;
		/** @var string $paymentMethod Признак способа расчёта */
		protected $paymentMethod;
		/** @var string $paymentObject Признак предмета расчёта */
		protected $paymentObject;

		/**
		 * @return mixed
		 */
		public function getPrice()
		{
			return $this->price;
		}

		/**
		 * @param mixed $price
		 * @return self
		 */
		public function setPrice($price)
		{
			$this->price = $price;
			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getQuantity()
		{
			return $this->quantity;
		}

		/**
		 * @param mixed $quantity
		 * @return self
		 */
		public function setQuantity($quantity)
		{
			$this->quantity = $quantity;
			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getName()
		{
			return $this->name;
		}

		/**
		 * @param mixed $name
		 * @return self
		 */
		public function setName($name)
		{
			$this->name = $name;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getVat()
		{
			return $this->vat;
		}

		/**
		 * @param string $vat
		 * @return self
		 */
		public function setVat($vat)
		{

			if(\in_array($vat, \array_keys(Payment::$vats)))
				$this->vat = $vat;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getPaymentMethod()
		{
			return $this->paymentMethod;
		}

		/**
		 * @param string $paymentMethod
		 * @return self
		 */
		public function setPaymentMethod($paymentMethod)
		{

			if($paymentMethod === '')
				$this->paymentMethod = '';

			if(\in_array($paymentMethod, \array_keys(Payment::$paymentMethods)))
				$this->paymentMethod = $paymentMethod;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getPaymentObject()
		{
			return $this->paymentObject;
		}

		/**
		 * @param string $paymentObject
		 * @return self
		 */
		public function setPaymentObject($paymentObject)
		{

			if($paymentObject === '')
				$this->paymentObject = '';

			if(\in_array($paymentObject, \array_keys(Payment::$paymentObjects)))
				$this->paymentObject = $paymentObject;

			return $this;
		}
	}