<?php

	/**
	 * Оплата заказа с передачей состава корзины в робокассу для печати чеков
	 * С новым алгоритмом устанавливаешим тип оплаты и вид оплаты
	 */

	require_once __DIR__ . '/autoload.php';

	/** @var \Robokassa\Order $order */
	$order = new \Robokassa\Order();

	/**
	 * Устанавливаем НДС
	 * @see \Robokassa\Payment::$vats
	 */
	$configure->setVat(\Robokassa\Payment::VAT_VAT18);

	/**
	 * Устанавливаем систему налогооблажения
	 * @see \Robokassa\Payment::$snos
	 */
	$configure->setSno(\Robokassa\Payment::SNO_USN_INCOME);

	/**
	 * Устанавливаем флаг отправки корзины для чека
	 * @see \Robokassa\Payment::$fiscalServices
	 */
	$configure->setFiscalService(\Robokassa\Payment::FISCAL_SERVICE_CLOUD);

	/**
	 * Установка номера зказа и его цены
	 */
	$order->setOrderId(2)
		->setPrice(1);

	/**
	 * Добавляем позиции в заказ
	 * Доставку добавляем так же
	 */
	$order
		->addProduct(
			(new \Robokassa\Product())
				->setName('Товар 1')
				->setQuantity(1)
				->setPrice(100)
				->setPaymentMethod(\Robokassa\Payment::PAYMENT_METHOD_FULL_PAYMENT)
				->setPaymentObject(\Robokassa\Payment::PAYMENT_OBJECT_COMMODITY)
		)
		->addProduct(
			(new \Robokassa\Product())
				->setName('Товар 2')
				->setQuantity(2)
				->setPrice(200)
				->setPaymentMethod(\Robokassa\Payment::PAYMENT_METHOD_CREDIT)
				->setPaymentObject(\Robokassa\Payment::PAYMENT_OBJECT_COMMODITY)
		)
	;

	/**
	 * Для конкретного товара можно выставить отдельно НДС
	 * @see \Robokassa\Product::setVat
	 */
	$order->addProduct(
		(new \Robokassa\Product())
			->setName('Доставка')
			->setQuantity(1)
			->setPrice(200)
			->setVat(\Robokassa\Payment::VAT_NONE)
			->setPaymentMethod(\Robokassa\Payment::PAYMENT_METHOD_PARTIAL_PAYMENT)
			->setPaymentObject(\Robokassa\Payment::PAYMENT_OBJECT_SERVICE)
	);


	/** @var array $payment Получаем массив параметров для оплаты */
	$payment = \Robokassa\Payment::getOrderPayFields($configure, $order);
	?>
	<form method='post' action='<?=\Robokassa\Helper::getPaymentUrl();?>'>
		<?php foreach($payment as $fieldCode => $fieldValue):?>
			<input type='hidden' name='<?=$fieldCode;?>' value='<?=$fieldValue;?>' />
		<?php endforeach;?>
		<input type='submit' value='Оплатить'>
	</form>