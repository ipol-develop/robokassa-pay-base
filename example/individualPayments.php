<?php

	/**
	 * Выбор платежной системы для оплаты заказ физ лица (с учетом комиссии)
	 */

	require __DIR__ . '/autoload.php';

	/** @var \Robokassa\Order $order */
	$order = new \Robokassa\Order();

	$order->setOrderId(1)
		->setPrice(100);

	$configure->setShopType(\Robokassa\Payment::SHOP_TYPE_INDIVIDUAL)
		->setWhoPayCommission(\Robokassa\Payment::WHO_PAY_COMMISSION_SHOP)
	;

	$payments = \Robokassa\Helper::getPaymentsWithOrderCommission($configure, $order);
?>

	<?php foreach($payments as $group):?>

		<h4><?=$group['name'];?></h4>
		<hr />

		<?php foreach ($group['items'] as $payment):?>
			<p>
				<a href="individual.php?IncCurrLabel=<?=$payment['label'];?>"><?=$payment['name'];?> - <?=$payment['price'];?></a>
			</p>
		<?php endforeach;?>
	<?php endforeach;?>