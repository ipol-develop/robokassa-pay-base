<?php

	/**
	 * Оплата простого заказа, с указанием валюты счета
	 */

	require_once __DIR__ . '/autoload.php';

	/** @var \Robokassa\Order $order */
	$order = new \Robokassa\Order();

	/**
	 * Установка номера зказа и его цены
	 */
	$order->setOrderId(2)
		->setPrice(100);

	/**
	 * Устанавлием валюту заказа - Тенге
	 */
	$configure->setOutSumCurrency(\Robokassa\Payment::OUT_SUM_CURRENCY_KZT);


	/** @var array $payment Получаем ссылку на оплату */
	$payment = \Robokassa\Payment::getOrderPayFields($configure, $order);
?>

<form method='post' action='<?=\Robokassa\Helper::getPaymentUrl();?>'>
	<?php foreach($payment as $fieldCode => $fieldValue):?>
		<input type='hidden' name='<?=$fieldCode;?>' value='<?=$fieldValue;?>' />
	<?php endforeach;?>
	<input type='submit' value='Оплатить'>
</form>