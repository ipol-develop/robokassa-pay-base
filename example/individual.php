<?php

	/**
	 * Оплата заказа для аккаунта физического лица
	 */

	require __DIR__ . '/autoload.php';

	/** @var \Robokassa\Order $order */
	$order = new \Robokassa\Order();

	$order->setOrderId(1)
		->setPrice(100);

	$configure->setShopType(\Robokassa\Payment::SHOP_TYPE_INDIVIDUAL)
		->setWhoPayCommission(\Robokassa\Payment::WHO_PAY_COMMISSION_SHOP)
		->setDefaultPayMethod($_GET['IncCurrLabel'])
	;

	$payments = \Robokassa\Helper::getPaymentsWithOrderCommission($configure, $order);

	/** @var array $payment Получаем массив параметров для оплаты */
	$payment = \Robokassa\Payment::getOrderPayFieldsIndividual($configure, $order);
?>
	<form method='post' action='<?=\Robokassa\Helper::getPaymentUrl();?>'>
		<?php foreach($payment as $fieldCode => $fieldValue):?>
			<input type='hidden' name='<?=$fieldCode;?>' value='<?=$fieldValue;?>' />
		<?php endforeach;?>
		<input type='submit' value='Оплатить'>
	</form>