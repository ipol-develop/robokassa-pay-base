<?php

    /**
     * Оплата простого заказа, только для Казахстана (отключаем фискализацию)
     */

    require_once __DIR__ . '/autoload.php';

    /** @var \Robokassa\Order $order */
    $order = new \Robokassa\Order();

    /**
     * Установка номера зказа и его цены
     */
    $order->setOrderId(2)
        ->setPrice(1);

    /**
     * Устанавлием страну магазина - Казахстан
     */
    $configure->setCountryCode(\Robokassa\Payment::COUNTRY_CODE_KZ);

    /** @var array $payment Получаем ссылку на оплату */
    $payment = \Robokassa\Payment::getOrderPayFields($configure, $order);
?>

    <form method='post' action='<?=\Robokassa\Helper::getPaymentUrl();?>'>
        <?php foreach($payment as $fieldCode => $fieldValue):?>
            <input type='hidden' name='<?=$fieldCode;?>' value='<?=$fieldValue;?>' />
        <?php endforeach;?>
        <input type='submit' value='Оплатить'>
    </form>