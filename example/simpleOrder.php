<?php

	/**
	 * Оплата простого заказа, используем только номер заказа, цену и формируем ссылку на робокассу для оплаты
	 */

	require_once __DIR__ . '/autoload.php';

	/** @var \Robokassa\Order $order */
	$order = new \Robokassa\Order();

	/**
	 * Установка номера зказа и его цены
	 */
	$order->setOrderId(2)
		->setPrice(1);

	/** @var string $payment Получаем ссылку на оплату */
	$payment = \Robokassa\Payment::getPayUrl($configure, $order);
?>
<a href="<?=$payment;?>">Оплатить заказ</a>