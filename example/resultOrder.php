<?php

	require_once __DIR__ . '/autoload.php';

    /** @var \Robokassa\Order $order */
    $order = new \Robokassa\Order();

	$order->setOrderId(2)
		->setPrice(1);

	/** @var \Robokassa\Response $response */
	$response = \Robokassa\Payment::processOrderResult(
		$configure,
		$order,
		\Robokassa\Payment::METHOD_POST
	);

	if($response->isPayed())
	{

		/**
		 * Обрабатываем заказ
		 * Объект заказа можно получить из ответа или использовать начальный
		 * Получение переданный дополнительных полдей @see \Robokassa\Response::getShpFields()
		 * Отвечаем успехом обработки
		 */

		echo $response->successResponse();
	}
	else
	{

		/**
		 * Отвечаем ошибкой обработки
		 */
		echo $response->failResponse();
	}