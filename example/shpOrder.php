<?php

	/**
	 * Оплата простого заказа, используем только номер заказа, цену и формируем ссылку на робокассу для оплаты
	 * Дополнительно передаем параметры SHP_ которые робокасса потом нам вернет в resultUrl
	 */

	require_once __DIR__ . '/autoload.php';

	/** @var \Robokassa\Order $order */
	$order = new \Robokassa\Order();

	/**
	 * Установка номера зказа и его цены
	 */
	$order->setOrderId(2)
		->setPrice(1)
		->setShpFields(
			[
				'a' => 'b',
			]
		);

	/** @var string $payment Получаем ссылку на оплату */
	$payment = \Robokassa\Payment::getPayUrl($configure, $order);
?>
<a href="<?=$payment;?>">Оплатить заказ</a>